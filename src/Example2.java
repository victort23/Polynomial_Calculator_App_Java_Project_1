
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class Example2 extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JPanel pane = new JPanel(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    private JButton button = new JButton("OK");
    private JTextField text = new JTextField(20);
    private JTextField text2 = new JTextField(20);
    private JTextField text3 = new JTextField(20);
    private JLabel exemplu = new JLabel("Forma polinomiala obligatorie:" +
            "(val)x^(exp)+(/)-(val)x+(/)-val");
    private JLabel label = new JLabel("Introduceti polinomul 1:" +
            " iar puneti o cifra in fata oricarui x");
    private JLabel label2 = new JLabel("Introduceti polinomul 2:");
    private JLabel label3 = new JLabel("Polinm pentru derivare/integrare:");

    private Logic logic = new Logic();
    private String txt;

    public Example2() {

        JFrame frame = new JFrame ("Calculator polinoame ");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300);
        JPanel panel1 = new JPanel();
        panel1.setBackground(Color.gray);
        panel1.setLayout(new FlowLayout());

        JButton b1 = new JButton("ADUNARE");
        JButton b2 = new JButton("SCADERE");

        JButton b3 = new JButton("DERIVARE");
        JButton b4 = new JButton("INTEGRARE");

        JButton b5 = new JButton("INMULTIRE");
        panel1.add(exemplu);
        panel1.add(label);
        panel1.add(text);
        panel1.add(label2);
        panel1.add(text2);
        panel1.add(label3);
        panel1.add(text3);
        panel1.add(b1);
        panel1.add(b2);
        panel1.add(b3);
        panel1.add(b4);
        panel1.add(b5);

        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JFrame frameRez = new JFrame ("Rezultat: ");
                frameRez.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frameRez.setSize(400, 110);
                JPanel panelRez = new JPanel();
                // panelStud.setLayout(new GridLayout(5,5));
                panelRez.setLayout(new FlowLayout());

                JLabel labelRez = new JLabel("Rezultatul adunarii este :");

                panelRez.add(labelRez);
                txt=text.getText();
                Polynom p1=new Polynom(txt);
                txt=text2.getText();
                Polynom p2=new Polynom(txt);
                Operatii op=new Operatii(p1,p2);
                JLabel label2 = new JLabel(op.Adunare().afis2());
                panelRez.add(label2);

                JButton inch= new JButton("Back");
                JPanel btnPnl = new JPanel(new BorderLayout());
                panelRez.add(inch);
                inch.addActionListener(new java.awt.event.ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        frameRez.setVisible(false);

                    }
                });
                frameRez.setContentPane(panelRez);
                frameRez.setVisible(true);
            }
        });
        b2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JFrame frameRez = new JFrame ("Rezultat: ");
                frameRez.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frameRez.setSize(400, 110);
                JPanel panelRez = new JPanel();
                // panelStud.setLayout(new GridLayout(5,5));
                panelRez.setLayout(new FlowLayout());

                JLabel labelRez = new JLabel("Rezultatul scaderii este :");

                panelRez.add(labelRez);
                txt=text.getText();
                Polynom p1=new Polynom(txt);
                txt=text2.getText();
                Polynom p2=new Polynom(txt);
                Operatii op=new Operatii(p1,p2);
                JLabel label2 = new JLabel(op.Scadere().afis2());
                panelRez.add(label2);

                JButton inch= new JButton("Back");
                JPanel btnPnl = new JPanel(new BorderLayout());
                panelRez.add(inch);
                inch.addActionListener(new java.awt.event.ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        frameRez.setVisible(false);
                    }
                });
                frameRez.setContentPane(panelRez);
                frameRez.setVisible(true);
            }
        });
        b3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JFrame frameRez = new JFrame ("Rezultat: ");
                frameRez.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frameRez.setSize(400, 110);
                JPanel panelRez = new JPanel();
                // panelStud.setLayout(new GridLayout(5,5));
                panelRez.setLayout(new FlowLayout());

                JLabel labelRez = new JLabel("Rezultatul derivarii este :");

                panelRez.add(labelRez);
                txt=text3.getText();
                Polynom p1=new Polynom(txt);

                Operatii op=new Operatii();
                JLabel label2 = new JLabel(op.Derivata(p1).afis2());
                panelRez.add(label2);

                JButton inch= new JButton("Back");
                JPanel btnPnl = new JPanel(new BorderLayout());
                panelRez.add(inch);
                inch.addActionListener(new java.awt.event.ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        frameRez.setVisible(false);
                    }
                });
                frameRez.setContentPane(panelRez);
                frameRez.setVisible(true);
            }
        });
        b4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JFrame frameRez = new JFrame ("Rezultat: ");
                frameRez.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frameRez.setSize(1200, 110);
                JPanel panelRez = new JPanel();
                // panelStud.setLayout(new GridLayout(5,5));
                panelRez.setLayout(new FlowLayout());

                JLabel labelRez = new JLabel("Rezultatul integrarii este :");

                panelRez.add(labelRez);
                txt=text3.getText();
                Polynom p1=new Polynom(txt);

                Operatii op=new Operatii();
                JLabel label2 = new JLabel(op.Integrala(p1).afis2());
                panelRez.add(label2);

                JButton inch= new JButton("Back");
                JPanel btnPnl = new JPanel(new BorderLayout());
                panelRez.add(inch);
                inch.addActionListener(new java.awt.event.ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        frameRez.setVisible(false);
                    }
                });
                frameRez.setContentPane(panelRez);
                frameRez.setVisible(true);
            }
        });

        b5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JFrame frameRez = new JFrame ("Rezultat: ");
                frameRez.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frameRez.setSize(1200, 110);
                JPanel panelRez = new JPanel();
                // panelStud.setLayout(new GridLayout(5,5));
                panelRez.setLayout(new FlowLayout());

                JLabel labelRez = new JLabel("Rezultatul inmultirii este :");

                panelRez.add(labelRez);
                txt=text.getText();
                Polynom p1=new Polynom(txt);
                txt=text2.getText();
                Polynom p2=new Polynom(txt);
                Operatii op=new Operatii(p1,p2);
                JLabel label2 = new JLabel(op.Inmultire().afis2());
                panelRez.add(label2);

                JButton inch= new JButton("Back");
                JPanel btnPnl = new JPanel(new BorderLayout());
                panelRez.add(inch);
                inch.addActionListener(new java.awt.event.ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        frameRez.setVisible(false);
                    }
                });
                frameRez.setContentPane(panelRez);
                frameRez.setVisible(true);
            }
        });
        frame.setContentPane(panel1);
        frame.setVisible(true);


    }




}
