public class Monom implements Comparable{
    private Double value;
    private int grade;

    public Monom(Double value,int grade)
    {
        this.value=value;
        this.grade=grade;
    }

    public Double getValue() {
        return value;
    }

    public int getGrade() {
        return grade;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public int compareTo(Object o) {
        Monom aux=(Monom) o;
        if(aux.getGrade()>this.grade)
            return 1;
        else
        if(aux.getGrade()==this.grade)
            return 0;
        else
            return -1;

    }
}
