import java.util.Collections;
import java.util.Scanner;
import java.util.Collections;
import java.util.LinkedList;
public class Operatii {
    Polynom rez=new Polynom();
    double val=0;
    int grad;
    Polynom p1;
    Polynom p2;
    Monom m;
    public Operatii(Polynom p11,Polynom p22)
    {

      /*  Scanner in=new Scanner(System.in);
        System.out.println("Dati primul polinom : ");
        String text=in.nextLine();
        p1=new Polynom(text);
        System.out.println("Dati al doilea polinom : ");
        text=in.nextLine();
        p2=new Polynom(text);
        System.out.println(p1.getPoly().get(0).getValue());
        System.out.println(p2.getPoly().get(0).getValue());*/
        p1=p11;
        p2=p22;

    }
    public Operatii()
    {

    }
    public Polynom formaFinala(Polynom p)
    {
        int i=0;
        rez=new Polynom();
        Polynom rez=new Polynom();
        double val=p.getPoly().get(i).getValue();
        int grad=p.getPoly().get(i).getGrade();
        i++;
        int ok=0;
        while(i <p.getPoly().size())
        {
            ok=0;

            if(p.getPoly().get(i).getGrade()==grad )
                val=val+p.getPoly().get(i).getValue();
            else
            {
                ok=1;
                Monom m=new Monom(val,grad);
                rez.add(m);
                val=p.getPoly().get(i).getValue();

                grad=p.getPoly().get(i).getGrade();
            }
            i++;
        }
        Monom m=new Monom(val,grad);
        rez.add(m);
        return rez;
    }
    public Polynom Adunare()
    {
        int i=0,j=0;  rez=new Polynom();
        p1=formaFinala(p1); p2=formaFinala(p2);
        while(i < p1.getPoly().size() || j < p2.getPoly().size()){
            if (i >= p1.getPoly().size())
            {
                val=p2.getPoly().get(j).getValue();     grad=p2.getPoly().get(j).getGrade();
                m=new Monom(val,grad);  rez.add(m);
                break;
            }
            else if (j >= p2.getPoly().size())
            {
                val=p1.getPoly().get(i).getValue(); grad=p1.getPoly().get(i).getGrade();
                m=new Monom(val,grad);  rez.add(m);
                break;
            }
            else if(p1.getPoly().get(i).getGrade()==p2.getPoly().get(j).getGrade())
            {
                val=p1.getPoly().get(i).getValue()+p2.getPoly().get(j).getValue();
                grad=p1.getPoly().get(i).getGrade();
                m=new Monom(val,grad);     rez.add(m);
                i++;    j++;
            }
            else if(p1.getPoly().get(i).getGrade()>p2.getPoly().get(j).getGrade())
            {
                m=new Monom(p1.getPoly().get(i).getValue(),p1.getPoly().get(i).getGrade());
                i++;
                rez.add(m);
            }
            else if(p1.getPoly().get(i).getGrade()<p2.getPoly().get(j).getGrade())
            {
                m=new Monom(p2.getPoly().get(j).getValue(),p2.getPoly().get(j).getGrade());
                j++;
                rez.add(m);
            }
        }
        return rez;
    }
    public Polynom Scadere() {
        int i = 0,j=0;
        rez=new Polynom();
        while (i < p1.getPoly().size() || j < p2.getPoly().size()) {

            if (i >= p1.getPoly().size()) {
                val = (p2.getPoly().get(j).getValue());      grad = p2.getPoly().get(j).getGrade();
                m = new Monom(val, grad);
                rez.add(m);
                break;
            } else if (j >= p2.getPoly().size()) {
                val = p1.getPoly().get(i).getValue();    grad = p1.getPoly().get(i).getGrade();
                m = new Monom(val, grad); rez.add(m);
                break;
            } else if (p1.getPoly().get(i).getGrade() == p2.getPoly().get(j).getGrade()) {

                val = p1.getPoly().get(i).getValue() - p2.getPoly().get(j).getValue();
                grad = p1.getPoly().get(i).getGrade();
                m = new Monom(val, grad);    rez.add(m);
                i++;      j++;
            } else if (p1.getPoly().get(i).getGrade() > p2.getPoly().get(j).getGrade()) {
                m = new Monom(p1.getPoly().get(i).getValue(), p1.getPoly().get(i).getGrade());
                i++;
                rez.add(m);
            } else if (p1.getPoly().get(i).getGrade() < p2.getPoly().get(j).getGrade()) {
                m = new Monom(( p2.getPoly().get(j).getValue()), p2.getPoly().get(j).getGrade());
                j++;
                rez.add(m);
            }
        }
        return rez;
    }

    public Polynom Integrala(Polynom p)
    {
        rez=new Polynom();
        int i=0;   p=formaFinala(p);
        while(i < p.getPoly().size())
        {
            grad = p.getPoly().get(i).getGrade()+1;
            val = p.getPoly().get(i).getValue() / grad;
            m = new Monom(val, grad);
            rez.add(m);
            i++;
        }
        return rez;
    }
    public Polynom Derivata(Polynom p)
    {
        rez=new Polynom();
        int i=0;
        p=formaFinala(p);
        while(i < p.getPoly().size())
        {
            if(p.getPoly().get(i).getGrade()==0)
            {
                val=0; grad=0;
            }
            else
            {
                grad = p.getPoly().get(i).getGrade()-1;
                val = p.getPoly().get(i).getValue() * (grad+1);
            }
            m = new Monom(val, grad);
            rez.add(m);
            i++;
        }
        return rez;
    }
    public Polynom Inmultire()
    {
        for(int i=0;i < p1.getPoly().size();i++)
        {
            for(int j=0;j < p2.getPoly().size();j++)
            {

                val = p1.getPoly().get(i).getValue() * p2.getPoly().get(j).getValue();
                grad = p1.getPoly().get(i).getGrade()+p2.getPoly().get(j).getGrade();
                m = new Monom(val, grad);
                rez.add(m);
            }
        }
        Collections.sort(rez.getPoly());
        rez=formaFinala(rez);
        return rez;
    }
    public Polynom Inmultire(Polynom a,Polynom b)
    {
        for(int i=0;i < a.getPoly().size();i++)
        {
            for(int j=0;j < b.getPoly().size();j++)
            {

                val = a.getPoly().get(i).getValue() * b.getPoly().get(j).getValue();
                grad = a.getPoly().get(i).getGrade()+b.getPoly().get(j).getGrade();
                m = new Monom(val, grad);
                rez.add(m);
            }
        }
        Collections.sort(rez.getPoly());
        rez=formaFinala(rez);
        return rez;
    }


    public Polynom Impartire()
    {
        Polynom aux=new Polynom();
        while(p1.getPoly().size()==0 && (p2.getPoly().get(0).getGrade()<=p1.getPoly().get(0).getGrade()))
        {
            val = p1.getPoly().get(0).getValue() /p2.getPoly().get(0).getValue();
            grad=p1.getPoly().get(0).getGrade()-p2.getPoly().get(0).getGrade();
            Monom m=new Monom(val,grad);
            rez.add(m);

            aux=this.Inmultire(rez,p2);
            Collections.sort(aux.getPoly());
            Operatii op1=new Operatii(p1,aux);
            p1=op1.Scadere();

        }
        return rez;
    }
}
