import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;
import static org.testng.AssertJUnit.assertEquals;

class OperatiiTest {

    @Test
    void formaFinala() {
        Polynom p1=new Polynom("3x^2+4x+6x+12+10");
        Polynom rez=new Polynom("3x^2+10x+22");
        Operatii op=new Operatii();
        assertEquals(rez.afis2(),op.formaFinala(p1).afis2());
    }

    @Test
    void adunare() {
        Polynom p1=new Polynom("3x^2+4x+10");
        Polynom p2=new Polynom("2x^2+1x+5");
        Polynom rez=new Polynom("5n^2+5n+15");
        Operatii op=new Operatii(p1,p2);
        assertEquals(rez.afis2(),op.Adunare().afis2());

    }

    @Test
    void scadere() {
        Polynom p1=new Polynom("3x^2+4x+10");
        Polynom p2=new Polynom("2x^2+1x+5");
        Polynom rez=new Polynom("1n^2+3n+5");
        Operatii op=new Operatii(p1,p2);
        assertEquals(rez.afis2(),op.Scadere().afis2());
    }

    @Test
    void integrala() {
        Polynom p1=new Polynom("x^3+3x+2");

        Operatii op=new Operatii();
        assertEquals(" + 0.25 * n^4 +  + 1.5 * n^2 +  + 2.0 * n ",op.Integrala(p1).afis2());
    }

    @Test
    void derivata() {
        Polynom p1=new Polynom("x^3+3x+2");
        Polynom rez=new Polynom("3n^2+3+0");
        Operatii op=new Operatii();
        assertEquals(rez.afis2(),op.Derivata(p1).afis2());
    }

    @Test
    void inmultire() {
        Polynom p1=new Polynom("3x^2+4x+10");
        Polynom p2=new Polynom("2x^2+1x+5");
        Polynom rez=new Polynom(" 6n^4+11*n^3+39*n^2+30 * n+50");
        Operatii op=new Operatii(p1,p2);
        assertEquals(rez.afis2(),op.Inmultire().afis2());
    }

}