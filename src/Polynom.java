import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Polynom {

    private LinkedList<Monom> poly;
    double val = 0;
    int grad=0;
    LinkedList<String> match=new LinkedList<String>();
    static String extractInt(String str) {
        str = str.replaceAll("[^\\-\\d]", " ");
        str = str.trim();
        str = str.replaceAll(" +", " ");

        if (str.equals(""))
            return "-1";
        return str;
    }
    public Polynom()
    {
        poly = new LinkedList<>();
    }
    public Polynom(String polinom) {

        poly = new LinkedList<>();
        Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
        Matcher matcher = pattern.matcher(polinom);
        int x = 0;
        while (matcher.find()) {
            match.add(matcher.group(1));
            x = x + 1;
        }
        for (int i = 0; i < match.size(); i++) {
            String str1 = match.get(i);

            String s = extractInt(str1);
            System.out.println(s);
            String[] arrOfStr = s.split(" ");
            if(arrOfStr.length==1) {
                if (str1.contains("x") == true && (str1.contains("^") == true) ||( str1.contains("n") == true && (str1.contains("^") == true))) {
                    grad = Integer.parseInt(arrOfStr[0]);
                    val = 1.0;
                } else if (str1.contains("x") == true && str1.contains("^") == false || (str1.contains("n") == true && str1.contains("^") == false)) {
                    val = Double.parseDouble(arrOfStr[0]);
                    grad = 1;
                } else {
                    val = Double.parseDouble(arrOfStr[0]);
                    grad = 0;
                }
            }
            else
            {
                if(arrOfStr.length==2)
                {
                    val = Double.parseDouble(arrOfStr[0]);
                    grad=Integer.parseInt(arrOfStr[1]);
                }
            }
            //  System.out.print("Grad ="+grad+"Val="+val);
            poly.add(new Monom(val, grad));
        }

    }

    public LinkedList<Monom> getPoly() {
        return poly;
    }
    public void add(Monom m)
    {
        poly.add(m);
    }

    @Override
    public String toString() {
        return "Polynom{" +
                "val=" + val +
                ", grad=" + grad +
                '}';
    }

    void afis() {
        for (int i = 0; i < poly.size() ; i++) {
            if(poly.get(i).getGrade()!=0)
            {
                if(poly.get(i).getGrade()>1 && poly.get(i).getValue()>0 )
                    System.out.print( " + " +poly.get(i).getValue()+" * n"+"^"+poly.get(i).getGrade()+" + ");
                else if((poly.get(i).getGrade()>1 && poly.get(i).getValue()<0))
                    System.out.print( poly.get(i).getValue()+" * n"+"^"+poly.get(i).getGrade()+" + ");
                else
                if(poly.get(i).getGrade()>1 && poly.get(i).getValue()==1)
                {
                    System.out.print(" + n"+"^"+poly.get(i).getGrade()+"  ");
                }
                else if (poly.get(i).getGrade()>1 && poly.get(i).getValue()==-1)
                    System.out.print(" -n"+"^"+poly.get(i).getGrade()+" ");
                else
                if(poly.get(i).getValue()>0 && poly.get(i).getGrade()==1)
                    System.out.print(" + " + poly.get(i).getValue()+" * n"+" ");
                else
                    System.out.print( poly.get(i).getValue()+" * n"+"  ");
            }

            else
            if(poly.get(i).getValue()>=0)
                System.out.print(" + " + poly.get(i).getValue());
            else
                System.out.print(poly.get(i).getValue());
        }
    }
    public String afis2() {
        String s="";
        for (int i = 0; i < poly.size() ; i++) {
            if(poly.get(i).getGrade()!=0)
            {
                if(poly.get(i).getGrade()>1 && poly.get(i).getValue()>0 )
                    s+= ( " + " +poly.get(i).getValue()+" * n"+"^"+poly.get(i).getGrade()+" + ");
                else if((poly.get(i).getGrade()>1 && poly.get(i).getValue()<0))
                    s+=( poly.get(i).getValue()+" * n"+"^"+poly.get(i).getGrade()+" + ");
                else
                if(poly.get(i).getGrade()>1 && poly.get(i).getValue()==1)
                {
                    s+=(" + n"+"^"+poly.get(i).getGrade()+"  ");
                }
                else if (poly.get(i).getGrade()>1 && poly.get(i).getValue()==-1)
                    s+=(" -n"+"^"+poly.get(i).getGrade()+" ");
                else
                if(poly.get(i).getValue()>0 && poly.get(i).getGrade()==1)
                    s+=(" + " + poly.get(i).getValue()+" * n"+" ");
                else
                    s+=( poly.get(i).getValue()+" * n"+"  ");
            }

            else
            if(poly.get(i).getValue()>=0)
                s+=(" + " + poly.get(i).getValue());
            else
                s+= " "+(poly.get(i).getValue());
        }
        return s;
    }
}

